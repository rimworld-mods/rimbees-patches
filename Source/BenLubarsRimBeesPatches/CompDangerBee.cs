﻿using Verse;

namespace BenLubarsRimBeesPatches
{
    public class CompDangerBee : ThingComp
    {
        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            parent.Map.GetComponent<BeeDangerManager>().Track(parent);
        }

        public override void PostDeSpawn(Map map)
        {
            map.GetComponent<BeeDangerManager>().Untrack(parent);
        }
    }
}
