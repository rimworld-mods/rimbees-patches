﻿using System;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;

namespace BenLubarsRimBeesPatches
{
    [StaticConstructorOnStartup]
    public class CompBeeHousePatches : ThingComp
    {
        internal static readonly Type Building_Beehouse = Type.GetType("RimBees.Building_Beehouse,RimBees");
        internal static readonly AccessTools.FieldRef<object, int> tickCounter = AccessTools.FieldRefAccess<int>(Building_Beehouse, "tickCounter");
        internal static readonly AccessTools.FieldRef<object, int> ticksToDays = AccessTools.FieldRefAccess<int>(Building_Beehouse, "ticksToDays");
        internal static readonly MethodInfo CalculateTheTicksAverage = Building_Beehouse.GetMethod("CalculateTheTicksAverage");
        internal static readonly AccessTools.FieldRef<object, bool> BeehouseIsExpectingQueens = AccessTools.FieldRefAccess<bool>(Building_Beehouse, "BeehouseIsExpectingQueens");
        internal static readonly AccessTools.FieldRef<object, bool> BeehouseIsExpectingBees = AccessTools.FieldRefAccess<bool>(Building_Beehouse, "BeehouseIsExpectingBees");
        internal static readonly AccessTools.FieldRef<object, ThingOwner> innerContainerQueens = AccessTools.FieldRefAccess<ThingOwner>(Building_Beehouse, "innerContainerQueens");
        internal static readonly AccessTools.FieldRef<object, ThingOwner> innerContainerDrones = AccessTools.FieldRefAccess<ThingOwner>(Building_Beehouse, "innerContainerDrones");
        internal static readonly AccessTools.FieldRef<object, bool> flagPlants = AccessTools.FieldRefAccess<bool>(Building_Beehouse, "flagPlants");

        private static readonly Material OutOfFuelMat = MaterialPool.MatFrom("UI/Overlays/OutOfFuel", ShaderDatabase.MetaOverlay);
        private static readonly Material DroneMat = MaterialPool.MatFrom("Things/Item/Bees/RB_Bee_Temperate_Drone", ShaderDatabase.MetaOverlay);

        internal static readonly Vector3 ProgressBarOffset = new Vector3(0f, 0.1f, -0.45f);
        internal static readonly Vector2 ProgressBarSize = new Vector2(0.55f, 0.1f);
        internal static readonly Material UnfilledMat = SolidColorMaterials.SimpleSolidColorMaterial(new Color(0.3f, 0.3f, 0.3f));
        internal static readonly Material FilledMat = SolidColorMaterials.SimpleSolidColorMaterial(new Color(0.9f, 0.85f, 0.2f));

        public override void PostDraw()
        {
            if (BenLubarsRimBeesPatches.showMissingBees)
            {
                var expectingQueens = BeehouseIsExpectingQueens(parent);
                var expectingDrones = BeehouseIsExpectingBees(parent);
                var queenContainer = innerContainerQueens(parent);
                var droneContainer = innerContainerDrones(parent);
                var missingPlant = !flagPlants(parent);

                int offset = 0;
                if ((queenContainer.NullOrEmpty() && !expectingQueens) || (droneContainer.NullOrEmpty() && !expectingDrones))
                {
                    RenderMissingFuelOverlay(DroneMat, ref offset);
                }
                else if (missingPlant)
                {
                    // only one "weird plant" is needed even if both bees require different plants, so if we got here, both are missing.
                    var weirdPlant = BenLubarsRimBeesPatches.GetWeirdPlantDef(queenContainer.FirstOrFallback()) ?? BenLubarsRimBeesPatches.GetWeirdPlantDef(droneContainer.FirstOrFallback());
                    if (weirdPlant != null)
                    {
                        var mat = MaterialPool.MatFrom(new MaterialRequest(weirdPlant.uiIcon, ShaderDatabase.MetaOverlay));
                        RenderMissingFuelOverlay(mat, ref offset);
                    }
                }
            }

            if (BenLubarsRimBeesPatches.showProgress)
            {
                var averageDays = (float)CalculateTheTicksAverage.Invoke(parent, null);
                if (averageDays > 0f)
                {
                    var progress = tickCounter(parent) / (ticksToDays(parent) * averageDays);
                    GenDraw.DrawFillableBar(new GenDraw.FillableBarRequest
                    {
                        center = parent.DrawPos + ProgressBarOffset,
                        size = ProgressBarSize,
                        fillPercent = progress,
                        margin = 0.1f,
                        rotation = Rot4.North,
                        filledMat = FilledMat,
                        unfilledMat = UnfilledMat,
                    });
                }
            }
        }

        // based on RimWorld.OverlayDrawer.RenderPulsingOverlayInternal and overloads of RenderPulsingOverlay
        private void RenderMissingFuelOverlay(Material mat, ref int offset)
        {
            var mesh = MeshPool.plane08;
            var drawPos = parent.TrueCenter();
            drawPos.x += offset * parent.RotatedSize.x * 0.25f;
            drawPos.y = AltitudeLayer.MetaOverlays.AltitudeFor(5f);
            offset++;

            var fade = (Mathf.Sin((Time.realtimeSinceStartup + 397f * (parent.thingIDNumber % 571)) * 4f) + 1f) * 0.5f;
            fade = 0.3f + fade * 0.7f;

            var fadedMat1 = FadedMaterialPool.FadedVersionOf(mat, fade);
            Graphics.DrawMesh(mesh, Matrix4x4.TRS(drawPos, Quaternion.identity, Vector3.one), fadedMat1, 0);

            var fadedMat2 = FadedMaterialPool.FadedVersionOf(OutOfFuelMat, fade);
            drawPos.y += 3f / 74f;
            Graphics.DrawMesh(mesh, Matrix4x4.TRS(drawPos, Quaternion.identity, Vector3.one), fadedMat2, 0);
        }
    }
}
