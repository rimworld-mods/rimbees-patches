﻿using System.Collections.Generic;
using Verse;

namespace BenLubarsRimBeesPatches
{
    public class BeeDangerManager : MapComponent
    {
        public HashSet<Thing> bees = new HashSet<Thing>();

        public BeeDangerManager(Map map) : base(map)
        {
        }

        internal void Track(Thing bee)
        {
            bees.Add(bee);
        }

        internal void Untrack(Thing bee)
        {
            bees.Remove(bee);
        }
    }
}
