﻿using System;
using HarmonyLib;
using Verse;

namespace BenLubarsRimBeesPatches
{
    [StaticConstructorOnStartup]
    public class CompHybridizationChamberPatches : ThingComp
    {
        internal static readonly Type Building_HybridizationChamber = Type.GetType("RimBees.Building_HybridizationChamber,RimBees");
        internal static readonly AccessTools.FieldRef<object, int> tickCounter = AccessTools.FieldRefAccess<int>(Building_HybridizationChamber, "tickCounter");
        internal static readonly AccessTools.FieldRef<object, int> ticksToDays = AccessTools.FieldRefAccess<int>(Building_HybridizationChamber, "ticksToDays");
        internal static readonly AccessTools.FieldRef<object, bool> hybridizationChamberFull = AccessTools.FieldRefAccess<bool>(Building_HybridizationChamber, "hybridizationChamberFull");

        public override void PostDraw()
        {
            if (BenLubarsRimBeesPatches.showProgress)
            {
                var progress = tickCounter(parent) / (ticksToDays(parent) * 3f);
                if (hybridizationChamberFull(parent))
                {
                    progress = 1f;
                }

                GenDraw.DrawFillableBar(new GenDraw.FillableBarRequest
                {
                    center = parent.DrawPos + CompBeeHousePatches.ProgressBarOffset,
                    size = CompBeeHousePatches.ProgressBarSize,
                    fillPercent = progress,
                    margin = 0.1f,
                    rotation = Rot4.North,
                    filledMat = CompBeeHousePatches.FilledMat,
                    unfilledMat = CompBeeHousePatches.UnfilledMat,
                });
            }
        }
    }
}
