﻿using System;
using System.Reflection;
using HarmonyLib;
using Verse;

namespace BenLubarsRimBeesPatches
{
    [HarmonyPatch]
    [StaticConstructorOnStartup]
    static class Building_HybridizationChamber_HybridizationChecker_Patch
    {
        internal static readonly Type Building_HybridizationChamber = Type.GetType("RimBees.Building_HybridizationChamber,RimBees");
        internal static readonly MethodBase HybridizationChecker = Building_HybridizationChamber.GetMethod("HybridizationChecker");
        internal static readonly MethodInfo GetAdjacentBeehouse = Building_HybridizationChamber.GetMethod("GetAdjacentBeehouse");

        public static MethodBase TargetMethod()
        {
            return HybridizationChecker;
        }

        public static void Postfix(string __result, Building __instance)
        {
            var adjacentBeehouse = GetAdjacentBeehouse.Invoke(__instance, new object[0]);
            var queen = CompBeeHousePatches.innerContainerQueens(adjacentBeehouse).FirstOrFallback();
            var drone = CompBeeHousePatches.innerContainerDrones(adjacentBeehouse).FirstOrFallback();
            var queenSpecies = BenLubarsRimBeesPatches.GetBeeSpecies(queen);
            var droneSpecies = BenLubarsRimBeesPatches.GetBeeSpecies(drone);
            GameComponent_KnownBees.Instance.LogAttempt(queenSpecies, droneSpecies, string.IsNullOrEmpty(__result) ? null : __result);
        }
    }
}
