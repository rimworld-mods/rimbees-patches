﻿using System;
using HarmonyLib;
using Verse;

namespace BenLubarsRimBeesPatches
{
    [StaticConstructorOnStartup]
    public class CompBroodChamberPatches : ThingComp
    {
        internal static readonly Type Building_BroodChamber = Type.GetType("RimBees.Building_BroodChamber,RimBees");
        internal static readonly AccessTools.FieldRef<object, int> tickCounter = AccessTools.FieldRefAccess<int>(Building_BroodChamber, "tickCounter");
        internal static readonly AccessTools.FieldRef<object, int> ticksToDays = AccessTools.FieldRefAccess<int>(Building_BroodChamber, "ticksToDays");
        internal static readonly AccessTools.FieldRef<object, int> daysTotal = AccessTools.FieldRefAccess<int>(Building_BroodChamber, "daysTotal");

        public override void PostDraw()
        {
            if (BenLubarsRimBeesPatches.showProgress)
            {
                var progress = tickCounter(parent) / (float)(ticksToDays(parent) * daysTotal(parent));
                GenDraw.DrawFillableBar(new GenDraw.FillableBarRequest
                {
                    center = parent.DrawPos + CompBeeHousePatches.ProgressBarOffset,
                    size = CompBeeHousePatches.ProgressBarSize,
                    fillPercent = progress,
                    margin = 0.1f,
                    rotation = Rot4.North,
                    filledMat = CompBeeHousePatches.FilledMat,
                    unfilledMat = CompBeeHousePatches.UnfilledMat,
                });
            }
        }
    }
}
