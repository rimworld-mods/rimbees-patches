﻿using System;
using System.Collections.Generic;
using HarmonyLib;
using HugsLib;
using HugsLib.Settings;
using RimBees;
using Verse;

namespace BenLubarsRimBeesPatches
{
    [StaticConstructorOnStartup]
    public class BenLubarsRimBeesPatches : ModBase
    {
        internal static readonly Type CompBees = Type.GetType("RimBees.CompBees,RimBees");
        internal static readonly Type CompProperties_Bees = Type.GetType("RimBees.CompProperties_Bees,RimBees");
        internal static readonly AccessTools.FieldRef<object, string> species = AccessTools.FieldRefAccess<string>(CompProperties_Bees, "species");
        internal static readonly AccessTools.FieldRef<object, string> comb = AccessTools.FieldRefAccess<string>(CompProperties_Bees, "comb");
        internal static readonly AccessTools.FieldRef<object, string> weirdplantneeded = AccessTools.FieldRefAccess<string>(CompProperties_Bees, "weirdplantneeded");

        public static SettingHandle<bool> showMissingBees { get; private set; }
        public static SettingHandle<bool> showProgress { get; private set; }
        public static SettingHandle<bool> beeDangerAlert { get; private set; }
        public static SettingHandle<bool> knownBees { get; private set; }

        private static ThingComp GetCompBees(Thing bee)
        {
            var withComps = (ThingWithComps)bee;
            var comps = withComps.AllComps;

            for (var i = 0; i < comps.Count; i++)
            {
                var c = comps[i];
                if (CompBees.IsInstanceOfType(c))
                {
                    return c;
                }
            }

            return null;
        }

        public static string GetBeeSpecies(Thing bee)
        {
            return species(GetCompBees(bee).props);
        }

        public static ThingDef GetWeirdPlantDef(Thing bee)
        {
            var weirdPlantName = weirdplantneeded(GetCompBees(bee).props);
            if (weirdPlantName == "no")
            {
                return null;
            }

            return DefDatabase<ThingDef>.GetNamed(weirdPlantName);
        }

        public override void DefsLoaded()
        {
            showMissingBees = Settings.GetHandle(
                "showMissingBees",
                "BenLubarsRimBeesPatches_showMissingBees_title".Translate(),
                "BenLubarsRimBeesPatches_showMissingBees_desc".Translate(),
                true
            );
            showProgress = Settings.GetHandle(
                "showProgress",
                "BenLubarsRimBeesPatches_showProgress_title".Translate(),
                "BenLubarsRimBeesPatches_showProgress_desc".Translate(),
                true
            );
            beeDangerAlert = Settings.GetHandle(
                "beeDangerAlert",
                "BenLubarsRimBeesPatches_beeDangerAlert_title".Translate(),
                "BenLubarsRimBeesPatches_beeDangerAlert_desc".Translate(),
                true
            );
            knownBees = Settings.GetHandle(
                "knownBees",
                "BenLubarsRimBeesPatches_knownBees_title".Translate(),
                "BenLubarsRimBeesPatches_knownBees_desc".Translate(),
                true
            );

            var processedCombs = new HashSet<ThingDef>();
            foreach (var list in DefDatabase<BeeListDef>.AllDefsListForReading)
            {
                var queenDef = DefDatabase<ThingDef>.GetNamed(list.beeQueenDef);
                var droneDef = DefDatabase<ThingDef>.GetNamed(list.beeDroneDef);
                AddBeeHyperlinks(queenDef, droneDef, processedCombs);
                AddBeeHyperlinks(droneDef, queenDef, processedCombs);
            }
        }

        private void AddBeeHyperlinks(ThingDef def, ThingDef other, HashSet<ThingDef> processedCombs)
        {
            if (def.descriptionHyperlinks == null)
            {
                def.descriptionHyperlinks = new List<DefHyperlink>();
            }

            def.descriptionHyperlinks.Add(other);

            foreach (var comp in def.comps)
            {
                if (CompProperties_Bees.IsInstanceOfType(comp))
                {
                    var combDef = DefDatabase<ThingDef>.GetNamed(comb(comp));
                    def.descriptionHyperlinks.Add(combDef);

                    var weirdPlantName = weirdplantneeded(comp);
                    if (weirdPlantName != "no")
                    {
                        def.descriptionHyperlinks.Add(DefDatabase<ThingDef>.GetNamed(weirdPlantName));
                    }

                    if (processedCombs.Add(combDef))
                    {
                        if (combDef.descriptionHyperlinks == null)
                        {
                            combDef.descriptionHyperlinks = new List<DefHyperlink>();
                        }

                        foreach (var product in combDef.butcherProducts)
                        {
                            combDef.descriptionHyperlinks.Add(product.thingDef);
                        }
                    }

                    break;
                }
            }
        }

        public override void MapLoaded(Map map)
        {
            GameComponent_KnownBees.Instance.BackfillOwnedBees(map);
        }
    }
}
